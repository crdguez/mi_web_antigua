---
title: 'Acogida'
subtitle: 'Incio de curso 20192020'
summary: "Inicio de curso 2019-2020"
authors:
- admin
tags:
- Inicio
categories:
- inicio
date: "2019-09-13T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/6wAGwpsXHE0)'
  focal_point: ""
  preview_only: false

---

Tenéis publicado el calendario de exámenes en la web del instituto. En concreto, [aquí](https://drive.google.com/file/d/1BzTfg-qG1Ci6331i07p8BW9Rw6A-tsxg/view)



{{< gdocs src="https://docs.google.com/document/d/1FKjcq7S4E9FSmzuEH3xzf_kHZfaoQmLMZcObJ-MGbLI/edit?usp=sharing" >}}

---
title: 'Acogida'
subtitle: 'Incio de curso 20192020'
summary: "Inicio de curso 2019-2020. [Presentación](https://crdguez.gitlab.io/slides/bac2_inicio/#/)"
authors:
- admin
tags:
- Inicio
categories:
- inicio
date: "2019-09-12T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/6wAGwpsXHE0)'
  focal_point: ""
  preview_only: false

---

Este año me toca ser tutor de 2º de Bachillerato.

[Enlace](https://crdguez.gitlab.io/slides/bac2_inicio/#/) a la presentación del día de acogida.

Desde aquí deseo lo mejor a mi alumnado en este curso tan relevante. Estoy convencido de que con el esfuerzo de todos conseguiremos alcanzar los objetivos que nos propongamos. ¡Ánimo y a por ello!
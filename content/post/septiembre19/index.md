---
title: 'Septiembre 2019'
subtitle: 'Exámenes de septiembre'
summary: "Calendario de [exámenes](https://drive.google.com/file/d/1BzTfg-qG1Ci6331i07p8BW9Rw6A-tsxg/view)"
authors:
- admin
tags:
- Examenes
categories:
- examenes
date: "2019-08-29T00:00:00Z"
lastmod: "2019-08-29T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/Wj3SFi2BoeY)'
  focal_point: ""
  preview_only: false

---

Tenéis publicado el calendario de exámenes en la web del instituto. En concreto, [aquí](https://drive.google.com/file/d/1BzTfg-qG1Ci6331i07p8BW9Rw6A-tsxg/view)
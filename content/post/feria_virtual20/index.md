---
title: 'UNIferia UNIZAR'
subtitle: 'Segunda feria virtual del sistema universitario español organizado por Crue Universidades Españolas'
summary: " [Del 17 al 28 de febrero, la plataforma ofrecerá a los estudiantes información sobre la oferta académica de Grado Universitario, los diferentes sistemas de acceso, becas y los pasos necesarios para poder cursar Estudios Superiores en 54 universidades.](https://uniferia.crue.org/#!home)"
authors:
- admin
tags:
- Inicio
categories:
- inicio
date: "2020-02-25T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: ''
  focal_point: ""
  preview_only: false

# slides: "presentacion_evau"
---

La Universidad de Zaragoza estará en UNIferia, la segunda feria virtual del sistema universitario español organizado por Crue Universidades Españolas. Los Grupos de Trabajo de los Servicios de Información y Orientación Universitaria (SIOU) han  puesto en marcha este salón virtual, una herramienta que mejora los canales de comunicación y garantiza la igualdad de oportunidades sin las limitaciones que tienen las tradicionales ferias de Educación Superior.

Del 17 al 28 de febrero, la plataforma ofrecerá a los estudiantes información sobre la oferta académica de Grado Universitario, los diferentes sistemas de acceso, becas y los pasos necesarios para poder cursar Estudios Superiores en 54 universidades.

 

Una plataforma virtual permitirá a los futuros universitarios vencer las fronteras geográficas y temporales, informarse e interactuar con los diferentes servicios de información y orientación de las instituciones participantes, con un formato moderno y accesible.

 

Conocer la oferta de los nuevos títulos de Grado, los diferentes sistemas de acceso a la Universidad, las opciones de becas o de internacionalización pueden ser factores decisivos para el futuro profesional de los jóvenes.

[Enlace](https://uniferia.crue.org/) a la feria virtual.




---
title: 'EVAU'
subtitle: 'Información sobre la EVAU'
summary: "La EVAU en la universidad de Zaragoza. [Presentación](https://crdguez.gitlab.io/slides/presentacion_evau/#/)"
authors:
- admin
tags:
- Inicio
categories:
- inicio
date: "2019-09-24T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: ''
  focal_point: ""
  preview_only: false

slides: "presentacion_evau"
---


[Enlace](https://crdguez.gitlab.io/slides/presentacion_evau/#/) a la presentación.


---
title: 'Jornada de Puertas Abiertas'
subtitle: 'Facultad de Ciencias - Universidad de Zaragoza'
summary: " [Jornada de puertas abiertas Facultad de Ciencias 2020](http://eventos.unizar.es/47832/detail/jornada-de-puertas-abiertas-facultad-de-ciencias-2020.html)"
authors:
- admin
tags:
- Inicio
categories:
- inicio
date: "2020-02-21T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: ''
  focal_point: ""
  preview_only: false

# slides: "presentacion_evau"
---

La Facultad de Ciencias de la Universidad de Zaragoza abre sus puertas a alumnos de bachillerato que quieran conocer sus titulaciones y sus instalaciones. La [jornada de puertas abiertas]((http://eventos.unizar.es/47832/detail/jornada-de-puertas-abiertas-facultad-de-ciencias-2020.html)), en la que pueden participar alumnos y familiares, tendrá lugar el próximo día 21 de marzo y para asistir a la misma será necesario realizar una inscripción por alumno antes del día 17 de marzo. Dicha inscripción es gratuita.

[Enlace](http://eventos.unizar.es/event_detail/47832/tickets.html) al formulario de inscripción.


<iframe width="100%" height="300px" src="http://eventos.unizar.es/api/widget_map_event/eyJpZCI6IjQ3ODMyIiwibGF0IjoiNDEuNjQxNTgyOSIsImxuZyI6Ii0wLjg5ODM0OTQ5OTk5OTk5NDgiLCJ0aXRsZSI6Ikpvcm5hZGEgZGUgcHVlcnRhcyBhYmllcnRhcyBGYWN1bHRhZCBkZSBDaWVuY2lhcyAyMDIwIiwicGxhY2UiOiJVbml2ZXJzaWRhZCBkZSBaYXJhZ296YS4gQ2FtcHVzIFNhbiBGcmFuY2lzY28uIDUwMDA5IFphcmFnb3phLiBFc3BhXHUwMGYxYSIsInBsYWNlX2RldGFpbCI6IiIsImV4dHJhX2RhdGEiOmZhbHNlfQ%3D%3D/eyJoZWFkc3R5bGVzIjpbIndpZGdldHNcL2V2ZW50X21hcC5jc3NwIiwibWFwc2RnaW9cL21hcHNkZ2lvLmNzc3AiXSwiaGVhZHNjcmlwdHMiOlsiXC9sYW5nXC9qc1wvbWFwXC90ZXh0X3N0cmluZ3MiLCJcL2pzXC93aWRnZXRzXC9tYXBzZGdpb1wvbWFwc2RnaW8uanMiXSwidHlwZSI6InJhdyIsImNvbnRyb2wiOiJ6b29tLCBsZXllbmQsIG1hcHR5cGUsIGRpcmVjdGlvbnNlYXJjaCwgcGxhY2VzZWFyY2gsIGZ1bGxzY3JlZW4iLCJtYXBfdHlwZSI6Imdvb2dsZSIsIndpZHRoIjoiMTAwJSIsImhlaWdodCI6IjMwMHB4Iiwiem9vbSI6IjE1IiwiaWZyYW1lZCI6ZmFsc2V9" frameborder="0" allowtransparency="true">
<a href="http://eventos.unizar.es/api/widget_map_event/eyJpZCI6IjQ3ODMyIiwibGF0IjoiNDEuNjQxNTgyOSIsImxuZyI6Ii0wLjg5ODM0OTQ5OTk5OTk5NDgiLCJ0aXRsZSI6Ikpvcm5hZGEgZGUgcHVlcnRhcyBhYmllcnRhcyBGYWN1bHRhZCBkZSBDaWVuY2lhcyAyMDIwIiwicGxhY2UiOiJVbml2ZXJzaWRhZCBkZSBaYXJhZ296YS4gQ2FtcHVzIFNhbiBGcmFuY2lzY28uIDUwMDA5IFphcmFnb3phLiBFc3BhXHUwMGYxYSIsInBsYWNlX2RldGFpbCI6IiIsImV4dHJhX2RhdGEiOmZhbHNlfQ%3D%3D/eyJoZWFkc3R5bGVzIjpbIndpZGdldHNcL2V2ZW50X21hcC5jc3NwIiwibWFwc2RnaW9cL21hcHNkZ2lvLmNzc3AiXSwiaGVhZHNjcmlwdHMiOlsiXC9sYW5nXC9qc1wvbWFwXC90ZXh0X3N0cmluZ3MiLCJcL2pzXC93aWRnZXRzXC9tYXBzZGdpb1wvbWFwc2RnaW8uanMiXSwidHlwZSI6InJhdyIsImNvbnRyb2wiOiJ6b29tLCBsZXllbmQsIG1hcHR5cGUsIGRpcmVjdGlvbnNlYXJjaCwgcGxhY2VzZWFyY2gsIGZ1bGxzY3JlZW4iLCJtYXBfdHlwZSI6Imdvb2dsZSIsIndpZHRoIjoiMTAwJSIsImhlaWdodCI6IjMwMHB4Iiwiem9vbSI6IjE1IiwiaWZyYW1lZCI6ZmFsc2V9">Enlace</a>
</iframe>


---
title: 'Notas de corte de Ciclos Formativos'
subtitle: 'Relación de notas de Ciclos formativos para la Comunidad de Aragón'
summary: "Información sobre las notas de corte de años anteriores para acceder a los ciclos formativos"
authors:
- admin
tags:
- ciclos formativos
categories:
- inicio
date: "2020-02-27T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: ''
  focal_point: ""
  preview_only: false

# slides: "presentacion_evau"
---

* Información del proceso de admisión y **notas de corte** para el **curso 19-20**: [enlace](https://www.centroseducativosaragon.es/Public/noticias_detalle.aspx?id=171)
* Notas de corte del **curso 17-18**: [enlace](http://www.iessantiagohernandez.com/notas-de-corte-del-curso-201718/)


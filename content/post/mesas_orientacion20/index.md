---
title: 'Seminarios de Orientación'
subtitle: 'Orientación sobre estudios de grado y ciclos formativos'
summary: "Jornadas de
Orientación dirigidas a los estudiantes de segundo curso de Bachillerato
o de Formación Profesional de la Comunidad Autónoma de Aragón)"
authors:
- admin
tags:
- Seminarios de orientación
categories:
- inicio
date: "2020-02-25T00:00:00Z"
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: ''
  focal_point: ""
  preview_only: false

# slides: "presentacion_evau"
---

Durante el mes de marzo se van a realizar las Jornadas de
Orientación dirigidas a los estudiantes de segundo curso de Bachillerato
o de Formación Profesional de nuestra Comunidad Autónoma que tienen que
decidir cuál va a ser su futuro inmediato en relación a la elección de sus estudios.
Dichas jornadas se celebran en el edificio Paraninfo de la Universidad de Zaragoza y en
la Escuela de Artes.
Para orientarles y ayudarles en su elección, se han diseñado cuatro mesas de
orientación en las que se informará y debatirá sobre los estudios de
grado de la Universidad de Zaragoza, así como la oferta de ciclos formativos de
grado superior.

Estas Jornadas de Orientación han sido diseñadas por el Vicerrectorado de
Estudiantes y Empleo de la Universidad de Zaragoza y el Departamento de
Educación, Cultura y Deporte del Gobierno de Aragón según el siguiente calendario:
 * CIENCIAS SOCIALES Y JURÍDICAS. 9 de marzo. Aula Magna (edificio Paraninfo).
 * ARTES Y HUMANIDADES. 11 de marzo. Salón de Actos Escuela de Arte.
 * INGENIERÍA Y ARQUITECTURA. 16 de marzo. Aula Magna (edificio Paraninfo).
 * CIENCIAS Y CIENCIAS DE LA SALUD. 18 de marzo. Aula Magna (edificio Paraninfo).

Estas jornadas serán retransmitidas en streaming. Comenzarán a las 18:00
horas, teniendo una duración máxima de dos horas.





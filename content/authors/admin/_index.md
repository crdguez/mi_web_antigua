---
# Display name
name: Carlos Rodríguez Jaso

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Profesor de Matemáticas

# Organizations/Affiliations
organizations:
- name: IES Pedro Cerrada
  url: "http://www.iespedrocerrada.org/p/blog-page_19.html"

# Short bio (displayed in user profile at end of posts)
#bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Matemáticas
- Python
- Cultura libre
- Cultura maker

education:
  courses:
  - course: Licenciado en Matemáticas
    institution: Universidad de Zaragoza
  - course: Ingeniero técnico en Informática de Gestión
    institution: UNED  

 

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:rodriguezjasoc@iespedrocerrrada.org"
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/crdguez
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/crdguez

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
#user_groups:
#- Researchers
#- Visitors
---

Soy profesor de de **matemáticas** en Educación Secundaria y Bachillerato. Actualmente desarrollo mi carrera docente en el Instituto Pedro Cerrada de Utebo (Zaragoza).

La mayor parte de la documentación que genero la publico con **licencia** libre en repositorios de *github* y *gitlab*. 
{{< figure src="/img/attribution-share-alike-creative-commons-license.png"  width="20%" >}}


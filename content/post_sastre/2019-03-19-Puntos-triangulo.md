---
eye_catch: null
mathjax: true
tags:
  - matemáticas
  - triángulo
  - !!python/unicode '1ESO'
  - !!python/unicode '2ESO'
  - !!python/unicode '3ESO'
  - !!python/unicode '4ESO'
title: Rectas y puntos notables del triángulo
url: /2019/03/19/Puntos-triangulo/
date: "2019-03-19T00:00:00Z"
---


Mediana
-------

La **mediana** es el segmento que va del punto medio de un lado al
vértice opuesto.

Al punto dónde se cortan las medianas de un triángulo se le llama
**baricentro** y constituye el centro de gravedad del polígono.



<img src="/img/baricentro.png" width="40%">

Mediatriz
---------

La **mediatriz** de un segmento es la recta perpendicular al punto
medio. Geométricamente son los puntos del plano que equidistan a ambos
extremos del segmento.

En un triángulo llamaremos mediatriz a la mediatriz de cada uno de los
lados.

El punto de corte de las tres mediatrices equidista a los tres vértices
del triángulo. Ha dicho punto se le llama **circuncentro** porque
permite circunscribir el triángulo en una circunferencia de centro dicho punto.

<img src="/img/circuncentro.png" width="40%">

Altura
------

Llamaremos **altura** de un triángulo al segmento perpendicular a un
lado y pasa por el vértice opuesto.

Al punto de corte de las alturas se le llama **ortocentro**.

<img src="/img/ortocentro.png" width="40%">

Bisectriz
---------

Llamamos **bisectriz** de un ángulo a la semirrecta que divide al ángulo
en dos ángulos iguales. En un triángulo tendremos las tres bisectrices
correspondientes a cada uno de los tres ángulos.

Llamaremos **incentro** al punto de corte de las bisectrices de un
triángulo.

<img src="/img/incentro.png" width="40%">
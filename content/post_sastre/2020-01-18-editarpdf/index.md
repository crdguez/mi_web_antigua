---
eye_catch: null
mathjax: true
tags:
  - pdf

title: "Editar pdf con PDF Escape"
subtitle: "Herramienta online para editar pdf"
date: "2020-01-18T00:00:00Z"
markup: mmark
---

Para editar archivos *pdf* he encontrado la herramienta online [PDF Escape](https://www.pdfescape.com/). Tiene buena pinta.

<img src="featured.png" width="20%">
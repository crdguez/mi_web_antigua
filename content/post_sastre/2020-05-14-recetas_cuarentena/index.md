---
eye_catch: null
mathjax: true
tags:
  - cuarentena, openboard, droidcam, ffmpeg

title: "Recetas durante la cuarentena"
subtitle: "Pasos que he seguido para conseguir hacer funcionar algunos de los recursos que he utilizado durante la cuarentena por COVID19"
date: "2020-05-13T00:00:00Z"
markup: mmark
---



## Modificar tamaños de vídeos y orientación con ffmpeg

Algunas recetas:

```
ffmpeg -i VID_20200317_133312.mp4  -vf "transpose=1" -b 100k 2bac_ccss_p60e27.mp4
```

"transpose=2,transpose=2" --> gira el vídeo 180º
```
ffmpeg -i p164e3c.mp4  -vf "transpose=2,transpose=2" -b 100k p164e3c_red.mp4
```

## Utilizar mi móvil Xiaomi como webcam por usb

A traves de la app Droidcam:

Activamos la cámara:

```
droidcam
```

Por otro lado:

```
adb devices
```

o

```
sudo adb start-server 
```

Para terminar adb:

```
adb kill-server
```

He habilitado el acceso a USB por adb gracias a:

https://www.androidsis.com/como-habilitar-las-opciones-de-desarrollador-en-xiaomi/

https://computingforgeeks.com/install-adb-fastboot-on-ubuntu-mint/

https://developer.android.com/studio/command-line/adb

He tenido que modificar /etc/rc.local  ---> añadir antes de exec 0


mv /dev/video1 /dev/video2
mv /dev/video0 /dev/video1
mv /dev/video2 /dev/video0

## Instalar OpenBoard

El paquete .deb que viene en la web oficial no me ha funcionado:

[https://openboard.ch/download.en.html](https://openboard.ch/download.en.html)

Lo he conseguido con *flathub*:

[https://askubuntu.com/questions/1128318/how-to-install-openboard-for-18-04](https://askubuntu.com/questions/1128318/how-to-install-openboard-for-18-04)






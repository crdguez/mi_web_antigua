---
eye_catch: null
mathjax: true
tags:
  - LaTeX

title: "Recetas LaTeX"
subtitle: "Mis recetas LaTeX"
date: "2020-02-15T00:00:00Z"
markup: mmark
---



Listado de recursos e instrucciones $\LaTeX$:

* **Ecuaciones en modo *display*:** En modo ecuación podemos iniciar la instrucción con * \\displaystyle *.  (Lo he encontrado en la siguiente [fuente](https://tex.stackexchange.com/questions/161260/displaystyle-package)). 
  * **Ejemplo:** $\displaystyle\lim_{x \to +\infty}(\sqrt{x}-\sqrt{x-1})$ que queda mejor que $\lim_{x \to +\infty}(\sqrt{x}-\sqrt{x-1})$. Para ello escribiremos: 

```
$\displaystyle\lim_{x \to +\infty}(\sqrt{x}-\sqrt{x-1})$
```

* 


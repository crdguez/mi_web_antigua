---
title: 'Extrayendo sonido de un video'
subtitle: 'Cómo extraer el sonido de un video con *ffmpeg*'
date: "2019-08-19"
featured: true
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/TMOeGZw9NY4)'
  focal_point: ""
  preview_only: false
---

Podemos reducir los videos: 

' ' ' 
ffmpeg -i multiplicacion_matrices.mp4 -b 100k multiplicacion_matrices_red.mp4
' ' '

{{< gist crdguez 2f114530df1b87f5e974e9f8ee721c90  >}}




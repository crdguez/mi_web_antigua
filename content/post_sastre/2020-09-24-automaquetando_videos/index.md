---
eye_catch: null
mathjax: true
tags:
  - moviepy, vídeos, edición, youtube

title: "Automatizando el proceso de edición de vídeos"
subtitle: "Explicación sobre cómo podemos automatizar el proceso de editar un vídeo con moviepy para añadirle un título y después subirlo directamente a nuestra cuenta de Youtube"
date: "2020-09-24T00:00:00Z"
markup: mmark
---

Teniendo en cuenta que desgraciadamente por las circunstancias actuales voy a tener que preparar vídeos con explicaciones y luego se pierde bastante tiempo subiendolos a YouTube se me ocurrió que sería una buena idea intentar automatizar todo lo que pueda el proceso. 

Además, ya puestos, como durante el curso pasado los vídeos que hice quedaron "bastante cutres", al menos ahora pretendo que tengan una portada con su título. Huelga decir que no pretendo hacer vídeos "superprofesionales", básicamente porque no soy youtuber. Me conformo con poder colgar material que pueda ser útil al alumnado en las circunstancias que nos encontramos.

El proceso que voy a explicar se divide en dos partes por tanto:

* Edición del vídeo con [MoviePy](https://zulko.github.io/moviepy/) para, en mi caso, añadir una portada . Si echas un vistazo a [MoviePy](https://zulko.github.io/moviepy/) verás que puedes hacer bastantes más cosas, pero por el momento me sirve lo anterior.  
* Subir el vídeo "editado" a mi cuenta de "Youtube"

## Añadir una portada con [MoviePy](https://zulko.github.io/moviepy/) a un vídeo que hayamos hecho

[MoviePy](https://zulko.github.io/moviepy/) es una librería de Python para editar vídeos. En mi caso voy a partir de una imagen que me servirá de fondo de portada. 

Algunas recetas:

```
ffmpeg -i VID_20200317_133312.mp4  -vf "transpose=1" -b 100k 2bac_ccss_p60e27.mp4
```

"transpose=2,transpose=2" --> gira el vídeo 180º
```
ffmpeg -i p164e3c.mp4  -vf "transpose=2,transpose=2" -b 100k p164e3c_red.mp4
```

## Utilizar mi móvil Xiaomi como webcam por usb

A traves de la app Droidcam:

Activamos la cámara:

```
droidcam
```

Por otro lado:

```
adb devices
```

o

```
sudo adb start-server 
```

Para terminar adb:

```
adb kill-server
```

He habilitado el acceso a USB por adb gracias a:

https://www.androidsis.com/como-habilitar-las-opciones-de-desarrollador-en-xiaomi/

https://computingforgeeks.com/install-adb-fastboot-on-ubuntu-mint/

https://developer.android.com/studio/command-line/adb

He tenido que modificar /etc/rc.local  ---> añadir antes de exec 0


mv /dev/video1 /dev/video2
mv /dev/video0 /dev/video1
mv /dev/video2 /dev/video0

## Instalar OpenBoard

El paquete .deb que viene en la web oficial no me ha funcionado:

[https://openboard.ch/download.en.html](https://openboard.ch/download.en.html)

Lo he conseguido con *flathub*:

[https://askubuntu.com/questions/1128318/how-to-install-openboard-for-18-04](https://askubuntu.com/questions/1128318/how-to-install-openboard-for-18-04)






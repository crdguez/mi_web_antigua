---
eye_catch: null
mathjax: true
tags:
  - 3D
  - Impresoras 3D
  - Ender 3
title: "Mis recetas Ender3"
subtitle: "Lo que voy haciendo para tener mi impresora 3D a punto"
date: "2019-08-28T00:00:00Z"
markup: mmark
---
La información de calibrado la he encontrado [aquí](https://all3dp.com/2/how-to-calibrate-a-3d-printer-simply-explained/)

De momento, la impresora se comporta bastante bien.

Con el comando *M503* vemos la configuración de pasos

Teóricamente la Ender 3 funciona con estos parámetros:

M92 X80.00 Y80.00 Z400.00 E93.00

Sin embargo para un cubo de 20x20x10 me daba las siguientes medidas:  X19.88 Y20.42 Z10.01

Mediante una sencialla regla de 3 vemos que el comando a ejecutar es: M92 X80.48 Y78.97

Y luego  M500 guardar configuración.

En definitiva:

```
M92 X80.48 Y78.97
M500
```
---
title: Reunión con padres
summary: Jornada de
authors: [Carlos Rodríguez]
tags: []
categories: []
date: "2019-10-02T00:00:00Z"
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: beige
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: github
  
  
---

{{< slide background-image="featured.jpg" >}}

# Reunión con padres

[Curso 19-20](https://crdguez.gitlab.io/slides/presentacion_bac2/#/)


---


### Introducción

- Carlos Rodríguez Jaso (Matemáticas)
- Justificación de la reunión
- Agradecer su presencia
- Hoja de firmas y hoja informativa

--- 
### Equipo docente


---

### Horario del grupo


---

### Calendario

- Festivos locales: 5 marzo y 19 junio
- Evaluaciones: 21-11, 20-02 y 21-05

---

### FUNCIONES DEL TUTOR:

- Coordinar la evaluación de los alumnos. Organizar las sesiones de evaluación.
- Facilitar la integración de los alumnos en el I.E.S.
- Orientar y asesorarlos sobre sus posibilidades académicas/profesionales.
- Informar a los padres de todo lo que les concierna (notas, faltas de asistencia...).
- Facilitar la colaboración familia-centro.

---

###  COLABORACIÓN FAMILIAS-CENTRO:

- Intercambio fluído de información pertinente y orientaciones.
- Trabajo común para fomentar hábitos adecuados (estudio, salud, etc).
- Cauces de participación (Siempre previa petición de hora para evitar aglomeraciones):

		* Tutor. Horario de atención a padres en I.E.S. y correo electrónico.
		* Profesorado de aula, cuando sea necesario, hora de atención a padres.
		* Departamento de Orientación (Cuestiones de Orientación profesional o dificultades de aprendizaje).
		* Equipo Directivo.
		* Consejo Escolar.
---

###  INFORMACIÓN SOBRE LA CLASE- GRUPO:

- 38 alumnos (SALUD y TECNOLÓGICO)
- Desdoble en troncales, optativas
- Faltas de asistencia:
	* Posibilidad de pérdida del derecho a la evaluación continua.
	* Consulta en SIGAD
	* Justificantes
- Alumnado con asignaturas pendientes

---

## EVAU

- [Web con información de la EVAU](https://academico.unizar.es/acceso-admision-grado/evau/evau)
- [Presentación realizada al alumnado](https://crdguez.gitlab.io/slides/presentacion_evau/#/)
- [Tríptico informativo](https://academico.unizar.es/sites/academico.unizar.es/files/archivos/acceso/folleto/folletoevau.pdf)

---

- Dudas, cuestiones
- ¡Gracias por vuestra asistencia!

---

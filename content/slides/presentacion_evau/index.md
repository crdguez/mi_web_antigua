---
title: EVAU
summary: 
authors: [Carlos Rodríguez]
tags: []
categories: []
date: "2019-09-24T00:00:00Z"
slides:
  # Choose a theme from https://github.com/hakimel/reveal.js#theming
  theme: white
  # Choose a code highlighting style (if highlighting enabled in `params.toml`)
  #   Light style: github. Dark style: dracula (default).
  highlight_style: dracula
---

# EVAU UNIV. ZARAGOZA

[EVALUACIÓN PARA EL ACCESO A LA UNIVERSIDAD](https://academico.unizar.es/acceso-admision-grado/evau/evau)

Se divide en dos fases:

|                      |                     |
| :------------------: | :-----------------: |
| __Fase Obligatoria__ | __Fase Voluntaria__ |



---

## Fase Obligatoria

Esta fase es obligatoria para los estudiantes de Bachillerato LOMCE que deseen acceder a estudios oficiales de grado y <span style="color:#FF0000"> _tiene validez indefinida_ </span> \. Está constituida por 4 ejercicios sobre las siguientes materias:

---

## Fase Obligatoria

- LENGUA CASTELLANA Y LITERATURA II

- HISTORIA DE ESPAÑA

- LENGUA EXTRANJERA II: INGLÉS,  FRANCÉS O ALEMÁN

Una asignatura troncal general de entre las que marcan modalidad en el Bachillerato:

- MATEMÁTICAS II, MATEMÁTICAS APLICADAS O LATÍN

---

## Fase Obligatoria

- Para los estudiantes de Bachillerato LOMCE es requisito haber cursado las materias de las que se examinen en esta fase.

- La calificación de la fase obligatoria será la media aritmética de las calificaciones de los cuatro ejercicios. 

- Esta calificación deberá ser igual o superior a 4 puntos para que pueda ser tenida en cuenta para el cálculo de la _nota de acceso_ .

---

## Nota de acceso

Esta nota determina el derecho del estudiante a acceder al sistema universitario y se calcula:

<span style="color:#FF0000"> __NOTA ACCESO =__ </span>

<span style="color:#FF0000"> __60% de la calificación final de Bachillerato__ </span>

<span style="color:#FF0000"> __\+__ </span>

<span style="color:#FF0000"> __40% de la calificación de la fase obligatoria__ </span>

El estudiante tendrá acceso a la universidad cuando su nota de acceso sea __igual o superior a cinco puntos\.__

---

## Fase voluntaria

Su finalidad es la de mejorar la nota de admisión, pudiendo llegar como máximo hasta los 14 puntos.

---

## Fase voluntaria

Se podrá examinar de hasta cuatro materias :

- De materias troncales de opción de 2º de Bachillerato
- De alguna de las materias troncales generales determinadas por la modalidad en el Bachillerato, siempre que no haya sido incluida en la Fase obligatoria

No se exige que el estudiante haya cursado en el Bachillerato las materias que elija.

---

## Nota de admisión

Permite mejorar la nota de acceso mediante la superación de determinadas materias de la EvAU que tengan un parámetro de ponderación asociado al estudio de grado solicitado.

---

## Nota de admisión

<span style="color:#FF0000">_NOTA DE ADMISIÓN =_</span>

<span style="color:#FF0000">_Nota acceso \+ a\*M_  _1_  _\+ b\*M_  _2_</span>

__M1, M2 =__ las calificaciones de un máximo de dos materias __superadas__ en laEvAUque proporcionen mejor nota de admisión para el gradosolicitado

__a, b__ = parámetros de [ponderación](https://academico.unizar.es/sites/academico.unizar.es/files/archivos/acceso/norma/ponder19205.pdf) de materias de la EvAU

---

## Nota de admisión

Una materia se considera aprobada con una nota mínima de 5 puntos\. Las materias suspendidas no se tienen en cuenta para el cálculo de la nota de admisión\.

- Las calificaciones de las materias M1 y M2  solo podrán ser aplicadas únicamente a los procedimientos de admisión a estudios de grado que se convoquen para acceder en los dos cursos académicos siguientes al de superación de las materias\.

---

{{< figure src="resumen2.jpg" title="Resumen" numbered="true" height="600" lightbox="true" >}}

---
## Para acabar ...

-  [Notas de corte](https://academico.unizar.es/acceso-admision-grado/corte)
-  **Preguntas, dudas, ...**
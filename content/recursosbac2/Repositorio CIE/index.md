---
draft: false
title: Repositorio de recursos CIE
summary: Carpeta con contenidos que vaya colgando a lo largo del curso
tags:
- Demo
date: "2016-09-13T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: https://github.com/crdguez/mat2bac_cit

image:
  caption: Photo by Antoine Dautry on Unsplash
  focal_point: Smart


---
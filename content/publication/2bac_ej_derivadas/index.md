---
title: "Ejercicios de derivadas de 2º Bachillerato"
authors:
- admin
date: "2019-10-22T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
#publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
#publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

tags:
- bachillerato2

featured: true

#url_pdf: "derivadas.pdf"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/s9CC2SKySJM)'
  focal_point: ""
  preview_only: false

links:
- icon: github
  icon_pack: fab
  name: notebooks
  url: "https://github.com/crdguez/mat1bac_cit/tree/master/notebooks"

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:


# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
#slides: "presentacion_bac2"
#url_slides: "https://crdguez.github.io/mis_presentaciones/cledu.html#/title-slide"
---

Publicadas las soluciones a los ejercicios de derivadass

{{% alert note %}}
Pinchando arriba en  el icono *pdf* accedes al documento o directamente de [aquí](https://crdguez.gitlab.io/publication/2bac_ej_derivadas/derivadas.pdf)
{{% /alert %}}



---
title: "Notebooks análisis ciencias"
authors:
- admin
date: "2019-10-03T00:00:00Z"
#doi: ""
draft: false

# Schedule page publish date (NOT publication's date).
#publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
#publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

#abstract: Presentación a padres
# Summary. An optional shortened abstract.
#summary: Diapositivas correspondientes a la charla con  padres

#links:
#- icon: github
#  icon_pack: fab
#  name: materiales
#  url: "https://github.com/crdguez"

tags:
- bachillerato2

featured: true

#url_pdf: "prueba.pdf"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/s9CC2SKySJM)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:


# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
#slides: "pres1920"
#url_slides: "https://crdguez.github.io/mis_presentaciones/cledu.html#/title-slide"
---

He ido creando los siguientes notebooks con algunos ejercicios de análisis:

**Notebook con soluciones a ejercicios de la EVAU:**

{{< gist crdguez deb16acc42652427d3f5f0bca3420aee>}}




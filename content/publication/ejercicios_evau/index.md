---
title: "Ejercicios EVAU"
authors:
- admin
date: "2019-08-09T00:00:00Z"
doi: ""

draft: true

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: Pruebas de matemáticas de la EVAU resueltas o con soluciones
# Summary. An optional shortened abstract.
summary: Toda la documentación está generada a partir del fantástico trabajo de  Julio García Galavis, que es la persona que ha resuelto los ejercicios y que los tiene en matemáticas en tu mundo. Muchas gracias Julio


tags:
- bachillerato2
- Source Themes
featured: true

#url_pdf: "prueba.pdf"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/s9CC2SKySJM)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:


# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: "presentacion_bac2"
#url_slides: "https://crdguez.github.io/mis_presentaciones/cledu.html#/title-slide"
---

{{% alert note %}}
Click the *Slides* button above to demo Academic's Markdown slides feature.
{{% /alert %}}

Supplementary notes can be added here, including [code and math](https://sourcethemes.com/academic/docs/writing-markdown-latex/).
